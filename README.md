# Project participants:
* Eduard Romanchuk
* Anhelina Abdumanova 
---
## We used the following technologies:
* Gulp - task manager for automatic execution of frequently used tasks.
* Gulp-sass - for convert code from SASS to CSS.
* Browser Sync - for browser synchronization.
* Gulp-uglify and Gulp-js-minify -for Minify JavaScript.
* Gulp-clean-css - for minify CSS.
* Gulp-clean - for remove files and folders.
* Gulp-concat - to merge files.
* Gulp-imagemin - for minify PNG, JPEG, GIF and SVG images.
* Gulp-autoprefixer - to manage browser prefixes in a project.
---
### Eduard Romanchuk tasks:
* assignment for student #2
* connected packages Gulp
### Anhelina Abdumanova tasks:
* asignment for student #1
* added Readme.md file


